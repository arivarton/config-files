" Vim color file - arivarton
" Name:       arivarton.vim
" Maintainer: https://github.com/arivarton/
" Url:        https://github.com/arivarton/vim-arivarton

set background=dark

if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = "arivarton"

" Palette
let s:bg = {'gui': '#000000', 'cterm': 232}
let s:black = {'gui': '#000000', 'cterm': 232}
let s:fg = {'gui': '#d0d0d0', 'cterm': 252}
let s:white = {'gui': '#ffffff', 'cterm': 15}
let s:light_gray = {'gui': '#bcbcbc', 'cterm': 250}
let s:gray = {'gui': '#808080', 'cterm': 244}
let s:dark_gray = {'gui': '#4e4e4e', 'cterm': 239}
let s:darker_gray = {'gui': '#262626', 'cterm': 235}
let s:cyan = {'gui': '#008787', 'cterm': 30}
let s:blue = {'gui': '#00afff', 'cterm': 39}
let s:dark_blue = {'gui': '#5f87ff', 'cterm': 69}
let s:dark_purple = {'gui': '#5f005f', 'cterm': 53}
let s:purple = {'gui': '#8787d7', 'cterm': 104}
let s:green = {'gui': '#87d700', 'cterm': 112}
let s:red = {'gui': '#af5faf', 'cterm': 133}
let s:bright_red = {'gui': '#ff0087', 'cterm': 198}
let s:orange = {'gui': '#d75f00', 'cterm': 166}
let s:yellow = {'gui': '#d7d75f', 'cterm': 185}
let s:light_pink = {'gui': '#d7afd7', 'cterm': 182}
let s:pink = {'gui': '#ff00d7', 'cterm': 200}
let s:dark_pink = {'gui': '#d700ff', 'cterm': 165}

"hi Directory 
exec "hi Boolean guifg=". s:white.gui ." guibg=NONE gui=bold ctermfg=". s:white.cterm
exec "hi Builtin guifg=". s:purple.gui ." guibg=NONE gui=bold ctermfg=". s:purple.cterm
exec "hi Character guifg=#ff9800 guibg=NONE gui=NONE ctermfg=208"
exec "hi Comment guifg=". s:gray.gui ." guibg=NONE gui=italic ctermfg=". s:gray.cterm
exec "hi Conditional guifg=". s:white.gui ." guibg=NONE gui=NONE cterm=bold ctermfg=". s:white.cterm
exec "hi ColorColumn guifg=NONE guibg=". s:dark_purple.gui ." gui=NONE ctermfg=NONE ctermbg=". s:dark_purple.cterm
exec "hi Constant guifg=". s:cyan.gui ." guibg=NONE gui=NONE ctermfg=". s:cyan.cterm
exec "hi cursorim guifg=#192224 guibg=#536991 gui=NONE ctermfg=235 ctermbg=60"
exec "hi CursorLine guifg=NONE guibg=". s:dark_gray.gui ." gui=NONE ctermfg=NONE ctermbg=". s:dark_gray.cterm
exec "hi CursorLineNr guifg=". s:pink.gui ." guibg=". s:dark_purple.gui ." gui=NONE ctermfg=". s:pink.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi CursorColumn guifg=NONE guibg=". s:dark_gray.gui ." gui=NONE ctermfg=NONE ctermbg=". s:dark_gray.cterm
exec "hi Cursor guifg=NONE guibg=". s:dark_gray.gui ." gui=NONE ctermfg=NONE ctermbg=241"
exec "hi Debug guifg=". s:yellow.gui ." guibg=NONE gui=NONE ctermfg=". s:yellow.cterm
exec "hi Define guifg=". s:pink.gui ." guibg=NONE gui=NONE ctermfg=". s:pink.cterm
exec "hi Delimiter guifg=". s:gray.gui ." guibg=NONE gui=NONE ctermfg=". s:gray.cterm
"hi EnumerationName 
"hi EnumerationValue 
exec "hi Error guifg=". s:bg.gui ." guibg=". s:red.gui ." gui=NONE ctermfg=". s:bg.cterm ." ctermbg=". s:red.cterm
exec "hi ErrorMsg guifg=". s:white.gui ." guibg=NONE gui=bold cterm=bold ctermfg=". s:white.cterm
exec "hi Exception guifg=". s:red.gui ." guibg=NONE gui=NONE ctermfg=". s:red.cterm
exec "hi Float guifg=". s:green.gui ." guibg=NONE gui=bold cterm=bold ctermfg=". s:green.cterm
"hi FoldColumn 
exec "hi Folded guifg=#a0a8b0 guibg=#384048 gui=NONE ctermfg=103 ctermbg=238"
exec "hi Function guifg=". s:dark_pink.gui ." guibg=NONE gui=NONE ctermfg=". s:dark_pink.cterm
exec "hi Identifier guifg=". s:fg.gui ." guibg=NONE gui=NONE cterm=NONE ctermfg=". s:fg.cterm
"hi Ignore 
exec "hi Import guifg=". s:white.gui ." guibg=NONE gui=NONE cterm=bold ctermfg=". s:white.cterm
exec "hi Include guifg=". s:white.gui ." guibg=NONE gui=NONE cterm=bold ctermfg=". s:white.cterm
exec "hi IncSearch guifg=". s:white.gui ." guibg=". s:dark_purple.gui ." gui=NONE ctermfg=". s:white.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi Keyword guifg=". s:white.gui ." guibg=NONE gui=NONE ctermfg=". s:white.cterm
exec "hi Label guifg=#7e8aa2 guibg=NONE gui=NONE ctermfg=103"
exec "hi LineNr guifg=". s:gray.gui ." guibg=". s:darker_gray.gui ." gui=NONE ctermfg=". s:gray.cterm ." ctermbg=". s:darker_gray.cterm
exec "hi Macro guifg=". s:gray.gui ." guibg=NONE gui=NONE ctermfg=". s:gray.cterm
exec "hi MatchParen guifg=". s:bg.gui ." guibg=". s:dark_blue.gui ." gui=bold cterm=bold ctermfg=". s:white.cterm ." ctermbg=". s:pink.cterm
exec "hi NonText guifg=#808080 guibg=#202020 gui=NONE ctermfg=8 ctermbg=234"
exec "hi Normal guifg=". s:fg.gui ." guibg=". s:bg.gui ." gui=NONE ctermfg=". s:fg.cterm ." ctermbg=". s:bg.cterm
exec "hi Number guifg=". s:light_pink.gui ." guibg=NONE gui=bold cterm=bold ctermfg=". s:light_pink.cterm
exec "hi Operator guifg=". s:white.gui ." guibg=NONE gui=NONE gui=bold cterm=bold ctermfg=". s:white.cterm
exec "hi PMenu guifg=". s:white.gui ." guibg=". s:dark_gray.gui ." gui=NONE ctermfg=". s:white.cterm ." ctermbg=". s:dark_gray.cterm
exec "hi PMenuSbar guifg=NONE guibg=". s:dark_gray.gui ." gui=NONE ctermfg=NONE ctermbg=". s:dark_gray.cterm
exec "hi PMenuSel guifg=#000000 guibg=". s:green.gui ." gui=NONE ctermfg=NONE ctermbg=". s:green.cterm
exec "hi PMenuThumb guifg=NONE guibg=". s:green.gui ." gui=NONE ctermfg=NONE ctermbg=". s:green.cterm
exec "hi PreCondit guifg=". s:gray.gui ." guibg=NONE gui=NONE ctermfg=". s:gray.cterm
exec "hi PreProc guifg=". s:white.gui ." guibg=NONE gui=NONE ctermfg=". s:white.cterm
exec "hi Question guifg=". s:yellow.gui ." guibg=NONE gui=bold cterm=bold ctermfg=". s:yellow.cterm
exec "hi Repeat guifg=". s:white.gui ." guibg=NONE gui=NONE cterm=bold ctermfg=". s:white.cterm
exec "hi Search guifg=". s:white.gui ." guibg=". s:pink.gui ." gui=NONE ctermfg=". s:white.cterm ." ctermbg=". s:pink.cterm

"hi SignColumn 
exec "hi Special guifg=". s:red.gui ." guibg=NONE gui=NONE ctermfg=". s:red.cterm
exec "hi SpecialChar guifg=". s:orange.gui ." guibg=NONE gui=NONE ctermfg=208"
exec "hi SpecialComment guifg=". s:orange.gui ." guibg=NONE gui=NONE ctermfg=208"
exec "hi SpecialKey guifg=". s:yellow.gui ." guibg=". s:dark_gray.gui ." gui=NONE ctermfg=". s:yellow.cterm ." ctermbg=". s:dark_gray.cterm
exec "hi Statement guifg=". s:white.gui ." guibg=NONE gui=NONE cterm=bold ctermfg=". s:white.cterm
exec "hi StatusLine guifg=". s:darker_gray.gui ." guibg=". s:pink.gui ." gui=NONE ctermfg=". s:darker_gray.cterm ." ctermbg=". s:pink.cterm
exec "hi StatusLineNC guifg=". s:gray.gui ." guibg=". s:dark_purple.gui ." gui=NONE ctermfg=". s:gray.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi StorageClass guifg=". s:dark_blue.gui ." guibg=NONE gui=NONE ctermfg=". s:dark_blue.cterm
exec "hi String guifg=". s:purple.gui ." guibg=NONE gui=NONE ctermfg=". s:light_pink.cterm
exec "hi Structure guifg=". s:bright_red.gui ." guibg=NONE gui=NONE ctermfg=". s:bright_red.cterm
exec "hi TabLine guifg=". s:bg.gui ." guibg=". s:darker_gray.gui ." gui=NONE ctermfg=". s:white.cterm ." ctermbg=". s:bg.cterm
exec "hi TabLineFill guifg=". s:bg.gui ." guibg=". s:fg.gui ." ctermfg=". s:bg.cterm ." ctermbg=". s:fg.cterm
exec "hi TabLineSel guifg=". s:pink.gui ." guibg=". s:dark_purple.gui ." gui=NONE ctermfg=". s:pink.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi Tag guifg=". s:orange.gui ." guibg=NONE gui=NONE ctermfg=208"
exec "hi Title guifg=". s:fg.gui ." guibg=NONE gui=bold cterm=bold ctermfg=". s:fg.cterm
exec "hi Todo guifg=". s:pink.gui ." guibg=NONE gui=NONE ctermfg=". s:pink.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi Type guifg=". s:purple.gui ." guibg=NONE gui=NONE ctermfg=". s:purple.cterm
exec "hi Typedef guifg=". s:dark_blue.gui ." guibg=NONE gui=NONE ctermfg=103"
"hi Underlined 
"hi Union 
exec "hi VertSplit guifg=NONE guibg=". s:dark_purple.gui ." gui=NONE ctermfg=". s:darker_gray.cterm ." ctermbg=". s:dark_purple.cterm
exec "hi Visual guifg=NONE guibg=". s:dark_gray.gui ." gui=NONE ctermfg=". s:white.cterm ." ctermbg=". s:dark_purple.cterm
"hi WarningMsg 
"hi WildMenu 
" Python
exec "hi pythonDecorator guifg=". s:green.gui ." guibg=NONE gui=NONE ctermfg=". s:green.cterm
exec "hi pythonDecoratorName guifg=". s:green.gui ." guibg=NONE gui=NONE ctermfg=". s:green.cterm
exec "hi pythonexception guifg=". s:bright_red.gui ." guibg=NONE gui=NONE gui=bold cterm=bold ctermfg=". s:bright_red.cterm
exec "hi pythonexclass guifg=". s:bright_red.gui ." guibg=NONE gui=NONE ctermfg=". s:bright_red.cterm

" HTML
exec "hi htmlTag guifg=". s:gray.gui ." guibg=NONE gui=NONE ctermfg=". s:gray.cterm
exec "hi htmlTagName guifg=". s:purple.gui ." guibg=NONE gui=NONE ctermfg=". s:purple.cterm
exec "hi htmlArg guifg=". s:white.gui ." guibg=NONE gui=NONE ctermfg=". s:white.cterm

" TODO: Add support for css

" Diff
exec "hi DiffAdd guifg=". s:black.gui ." guibg=". s:green.gui ." ctermfg=". s:black.cterm ." ctermbg=". s:green.cterm
exec "hi DiffChange guifg=". s:black.gui ." guibg=". s:blue.gui ." ctermfg=". s:black.cterm ." ctermbg=". s:blue.cterm
exec "hi DiffDelete guifg=". s:black.gui ." guibg=". s:red.gui ." ctermfg=". s:black.cterm ." ctermbg=". s:red.cterm
exec "hi DiffText guifg=". s:black.gui ." ctermfg=". s:black.cterm
