# Autogenerated config.py
# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# Uncomment this to still load settings configured via autoconfig.yml
# config.load_autoconfig()

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'file://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'qute://*/*')

# Change download directory
config.set('downloads.location.directory', '~/downloads/')

# Page(s) to open at the start.
# Type: List of FuzzyUrl, or FuzzyUrl
c.url.start_pages = ['https://slashdot.org/']

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = '#FFFFFF'

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
c.colors.hints.bg = '#8d32a8'

# Background color for prompts.
# Type: QssColor
c.colors.prompts.bg = '#570071'

# Foreground color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.fg = '#9a76a1'

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = '#000000'

# Foreground color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.fg = '#9a76a1'

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = '#000000'

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = '#570071'

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = '#570071'

# Font used in the tab bar.
c.fonts.default_family = 'iosevka term'
c.fonts.default_size = '14pt'

# Bindings for normal mode
config.bind('J', 'tab-prev')
config.bind('K', 'tab-next')

c.aliases.update({'clean-cache': 'spawn --userscript clean_cache'})
