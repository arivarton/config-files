syntax on
set nocompatible


" -------------------------------------------------------------------------------------------------
"                                            PLUGINS START

" Disable plugins in regular vim
if has('nvim')
    call plug#begin()
    """
"                                    Installed plugins
"   _______________________________________________________________________________________|
"  | FZF
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"   _______________________________________________________________________________________|
"  | Nerdcommenter
    Plug 'scrooloose/nerdcommenter'
"   _______________________________________________________________________________________|
"  | NCM2
    Plug 'ncm2/ncm2'
    " ncm2 requirements:
    Plug 'roxma/nvim-yarp'
    " Completion sources for ncm2
    Plug 'ncm2/ncm2-jedi'
    Plug 'ncm2/ncm2-html-subscope'
"   _______________________________________________________________________________________|
"  | Gitgutter
    Plug 'airblade/vim-gitgutter'
"   _______________________________________________________________________________________|
"  | Syntastic
    Plug 'vim-syntastic/syntastic'
"   _______________________________________________________________________________________|
"  | Snippets
    Plug 'ncm2/ncm2-ultisnips'
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'
"   _______________________________________________________________________________________|
"  | Nerdtree
    Plug 'scrooloose/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'
"   _______________________________________________________________________________________|
"  | Virtual env integration
    Plug 'plytophogy/vim-virtualenv'
"   _______________________________________________________________________________________|
"  | Fugitive
    Plug 'tpope/vim-fugitive'
"   _______________________________________________________________________________________|
"  | Mardown table of content creator
    Plug 'mzlogin/vim-markdown-toc'
"   _______________________________________________________________________________________|
"  | Buffer close
    Plug 'Asheq/close-buffers.vim'
"   _______________________________________________________________________________________|
"  | Arduino
    Plug 'stevearc/vim-arduino'
"   _______________________________________________________________________________________|
"  | Vimtex
    Plug 'lervag/vimtex'
"   _______________________________________________________________________________________|
"  | vim-ipython-cell
    Plug 'jpalardy/vim-slime', { 'for': 'python' }
    Plug 'hanschen/vim-ipython-cell', { 'for': 'python' }
    """
    call plug#end()

"                                    Plugin configuration
"   _______________________________________________________________________________________|
"  | scrooloose/nerdcommenter
    let g:NERDSpaceDelims = 1
    let g:NERDCompactSexyComs = 1
    let g:NERDDefaultAlign = 'left'
    let g:NERDTrimTrailingWhitespace = 1
    let g:NERDRemoveExtraSpaces = 1
"   _______________________________________________________________________________________|
"  | ncm2/ncm2
    " enable ncm2 for all buffers
    autocmd BufEnter * call ncm2#enable_for_buffer()
    " IMPORTANTE: :help Ncm2PopupOpen for more information
    set completeopt=noinsert,menuone,noselect
    " suppress the annoying 'match x of y', 'The only match' and 'Pattern not found' messages
    set shortmess+=c
    inoremap <c-c> <ESC>
    " When the <Enter> key is pressed while the popup menu is visible, it only
    " hides the menu. Use this mapping to close the menu and also start a new
    " line.
    inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
    " Use <TAB> to select the popup menu:
    inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
    inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"   _______________________________________________________________________________________|
"  | GitGutter
    set updatetime=100
"   _______________________________________________________________________________________|
"  | vary.vim
    let g:auto_striptrail = 'python,ruby'
    let g:auto_striptab = 'python,ruby,cpp'
"   _______________________________________________________________________________________|
"  | vim-airline
    let g:airline_powerline_fonts = 1
    if !exists('g:airline_symbols')
      let g:airline_symbols = {}
    endif
    let g:airline_symbols.space = "\ua0"
"   _______________________________________________________________________________________|
"  | VIM easy-align
    xnoremap ga <Plug>(EasyAlign)
    nnoremap ga <Plug>(EasyAlign)
    set printoptions=syntax:n,number:y
"   _______________________________________________________________________________________|
"  | Syntastic
    let g:syntastic_always_populate_loc_list = 1
    let g:syntastic_auto_loc_list = 1
    let g:syntastic_loc_list_height = 3
    let g:syntastic_check_on_open = 0
    let g:syntastic_check_on_wq = 0
    let g:syntastic_python_checkers = ['pylint']
    " Make pylint compatible with django
    let g:syntastic_python_pylint_args = "--load-plugins pylint_django"
    let g:syntastic_python_pylint_quiet_messages = {
        \ "type":  "style",
        \ "regex": ['\[missing-docstring\]',
        \           '\[too-many-ancestors\]',
        \           '\[ungrouped-imports\]'] }
    set vb
"   _______________________________________________________________________________________|
"  | Snippets
    " Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
    let g:UltiSnipsExpandTrigger="<c-x>"
    let g:UltiSnipsListSnippets="<c-s>"
    let g:UltiSnipsJumpForwardTrigger="<c-j>"
    let g:UltiSnipsJumpBackwardTrigger="<c-k>"
    " If you want :UltiSnipsEdit to split your window.
    let g:UltiSnipsEditSplit="vertical"
"   _______________________________________________________________________________________|
"  | NERDTree
    let NERDTreeShowHidden=1
    let NERDTreeIgnore=['^.ropeproject$', 
                \ '^__pycache__$',
                \ '^.git$',
                \ '\~$']
    let NERDTreeMapJumpLastChild="<M-j>"
    let NERDTreeMapJumpFirstChild="<M-k>"
    noremap <C-n> :NERDTreeFocus<cr>
"   _______________________________________________________________________________________|
"  | Virtualenv
    set statusline^=\[%{virtualenv#statusline()}\]\ 
"   _______________________________________________________________________________________|
"  | vim mardown table of contents
    let g:vmt_cycle_list_item_markers = 1
"   _______________________________________________________________________________________|
"  | Arduino
    let g:arduino_dir = '/usr/share/arduino'
    let g:arduino_cmd = '/usr/share/arduino/arduino'
    let g:arduino_board = 'archlinux-arduino:avr:uno'
    " function! ArduinoStatus()
    "   return g:arduino_board
    " endfunction
"   _______________________________________________________________________________________|
"  | vimtex
    let g:tex_flavor = 'latex'
"   _______________________________________________________________________________________|
"  | vim-ipython-cell
    let g:slime_python_ipython = 1
    " map <Leader>r to run script
    nnoremap <Leader>r :IPythonCellRun<CR>

endif
"                                            PLUGINS END                                           
" -------------------------------------------------------------------------------------------------
"                                            KEYMAPS

nnoremap K gt
nnoremap J gT
noremap <C-q> q
noremap <Enter> o<Esc>k
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>
nnoremap <space> za
" Window resizing
nnoremap + :exe "resize " . (winheight(0) * 3/2)<CR> 
nnoremap <kPlus> :exe "resize " . (winheight(0) * 3/2)<CR> 
nnoremap - :exe "resize " . (winheight(0) * 2/3)<CR> 
nnoremap <kMinus> :exe "resize " . (winheight(0) * 2/3)<CR> 
nnoremap <leader>o <C-w>_<C-w>\|
" Automatically change to base directory for each file
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>
" set rtp+=~/.fzf
" FZF keymaps
nnoremap <leader>fs :FZF!<CR>
nnoremap <leader>fdj :FZF $HOME/source/django/<CR>
nnoremap <leader>fdev :FZF $HOME/Development/<CR>
nnoremap <leader>fh :FZF $HOME<CR>
nnoremap <leader>fpy :FZF /usr/lib/python3.8/<CR>
" Use the 0 register (yank) instead of 1-9 register (delete) to paste
" noremap p "0p
nnoremap <F4> :wa<Bar>exe "mksession! " . v:this_session<CR>:echo 'Saved session here ' . v:this_session<CR> 

" diff
noremap <leader>dgl :diffget LO<CR> 
noremap <F17> :diffget<CR> 
noremap <leader>dgr :diffget RE<CR> 
noremap <F19> :diffput<CR> 
noremap <leader>du :diffupdate<CR> 
noremap <F18> :diffupdate<CR> 
nnoremap <F20> :GitGutterNextHunk<cr>
nnoremap <F16> :GitGutterPrevHunk<cr>

" Edit vimrc in split
nnoremap <leader>ev :tabnew $HOME/.vimrc<cr>
" Source vimrc
nnoremap <leader>sv :source $MYVIMRC<cr>

""" Terminal mappings
tnoremap <Esc> <C-\><C-n>

" New tab
noremap <F6> :tabnew<cr>
noremap <F7> :saveas 

" Buffer
noremap <F12> :bNext<cr>
noremap <F13> :bnext<cr>

" Update file
nnoremap <leader>u :edit %<cr>

"   _______________________________________________________________________________________
"  | Arduino
nnoremap <buffer> <leader>am :ArduinoVerify<CR>
nnoremap <buffer> <leader>as :ArduinoSerial<CR>
nnoremap <buffer> <leader>au :ArduinoUpload<CR>
nnoremap <buffer> <leader>ad :ArduinoUploadAndSerial<CR>
nnoremap <buffer> <leader>ab :ArduinoChooseBoard<CR>
nnoremap <buffer> <leader>ap :ArduinoChooseProgrammer<CR>
command! ArduinoKillSerial execute "\! kill $(ps -aux | grep '/dev/ttyACM0 9600' | awk '$11 \!~ /grep/ {print $2}')"
nnoremap <buffer> <leader>ak :ArduinoKillSerial<CR>

"                                            KEYMAPS END                                           
" -------------------------------------------------------------------------------------------------
"                                            FUNCTIONS

function! GitCurrentTag()
  return system("git describe --tags 2>/dev/null | tr -d '\n'")
endfunction

function! GitCurrentBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! WorkingDirName()
  return system("basename $(pwd) 2> /dev/null | tr -d '\n'")
endfunction

"                                            FUNCTIONS END                                           
" -------------------------------------------------------------------------------------------------
"                                            CONFIGURATIONS

"   _______________________________________________________________________________________
"  | Abbreviations
iabbrev atrygg Andri Viðar Tryggvason
iabbrev avt arivarton
"   _______________________________________________________________________________________|
"  | autocmd's
""" tex mappings
" autocmd FileType tex nnoremap <leader>ll<CR> :!texi2pdf %<CR>
" autocmd BufWritePost *.tex :!texi2pdf %
""" Tablengths
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType scss,css setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType arduino setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType tex setlocal shiftwidth=2 tabstop=2 softtabstop=2
""" Set filetype of .xsh
autocmd BufRead,BufNewFile *.xsh setfiletype python
""" Close nameless buffers on startup
autocmd VimEnter * Bdelete nameless
"   _______________________________________________________________________________________|
"  | set's
set encoding=utf-8
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set showcmd
set timeoutlen=2000
set clipboard=unnamedplus
set number
set sessionoptions=blank,buffers,curdir,folds,help,tabpages,winsize
set relativenumber
"   _______________________________________________________________________________________|
"  | Status line
set statusline=%{WorkingDirName()}
set statusline+=\ (%{GitCurrentBranch()}):
set statusline+=\ %{GitCurrentTag()}
set statusline+=\ \ \|\ \ %<%f\ %y%m%r%=%-14.(%l,%c%V%)\ %P
" Syntastic
set statusline+=\ 
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*


""" NVIM
" When using virtual environments, continue using system python with nvim
let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
