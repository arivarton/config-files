# If you come from bash you might have to change your $PATH.
# lkjsdflkjlkvbccvbjkhfdg23457lkjdfsdf
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/vidr/.oh-my-zsh

export BROWSER='qutebrowser'
export TERM='alacritty'
export COLORTERM='alacritty'
export VISUAL='nvim'
export EDITOR='nvim'
export CDPATH=$HOME/development/python/:$HOME/development/www/Django
export PYTHONDONTWRITEBYTECODE=1
# export PYTHONPATH='/home/vidr/.virtualenvs/arivarton-wagtail/lib/python3.6/site-packages/'
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Stamp env variables
export STAMP_MINIMUM_HOURS='1'
export STAMP_ORG_NR='919009551'
export STAMP_NAME='arivarton - Andri Vidar Tryggvason'
export STAMP_ADDRESS='Klepp stasjon'
export STAMP_ZIP_CODE='4353'
export STAMP_PHONE='+354 833 9005'
export STAMP_MAIL='faktura@arivarton.com'
export STAMP_ACCOUNT_NUMBER='9685 27 95848'
export STAMP_WAGE_PER_HOUR='350'

# To make the switch of cmd and write mode in vi mode faster
export KEYTIMEOUT=1
setopt nobeep

# ZSH Options
DISABLE_AUTO_UPDATE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"
# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git sudo wd systemd)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi


# Virtualenvwrapper
export WORKON_HOME=~/.virtualenvs
source /usr/bin/virtualenvwrapper_lazy.sh

# Ekrubrygg
ekrubrygg () {
    cd ~/development/www/Django/ekrubrygg/
    source /usr/bin/virtualenvwrapper.sh
    workon ekrubrygg
}

note-dev () {
    cd ~/development/python/note/
    source /usr/bin/virtualenvwrapper.sh
    workon note
}

tpc-dev () {
    cd ~/development/arduino/sketches/tire_pressure_controller/
}


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias vim='nvim'

bindkey -v
bindkey -M viins '\e.' insert-last-word

vim_ins_mode="$BG[015]$FG[232] INS %{$reset_color%}"
vim_cmd_mode="$BG[053]$FG[200] CMD %{$reset_color%}"
vim_mode=$vim_ins_mode

function zle-keymap-select {
vim_mode="${${KEYMAP/vicmd/${vim_cmd_mode}}/(main|viins)/${vim_ins_mode}}"
zle reset-prompt
}
zle -N zle-keymap-select

function zle-line-finish {
vim_mode=$vim_ins_mode
}
zle -N zle-line-finish

# Fix a bug when you C-c in CMD mode and you'd be prompted with CMD mode indicator, while in fact you would be in INS mode
# Fixed by catching SIGINT (C-c), set vim_mode to INS and then repropagate the SIGINT, so if anything else depends on it, we will not break it
# Thanks Ron! (see comments)
function TRAPINT() {
    vim_mode=$vim_ins_mode
    return $(( 128 + $1 ))
}

mountpoint='/mnt/ekrusida-server/'
function startvpn {
    sudo mkdir $mountpoint 2> /dev/null
    sudo rc-service openvpn start
    sudo mount -t nfs 10.6.9.1:/home/files/ $mountpoint
}
function stopvpn {
    sudo umount $mountpoint 2> /dev/null
    sudo rmdir $mountpoint 2> /dev/null
    sudo rc-service openvpn stop
}

function kill-aserial {
    kill $(ps -aux | grep '/dev/ttyACM0 9600' | awk '$11 !~ /grep/ {print $2}')
}

RPROMPT='${vim_mode}'

if [[ -n "$SSH_CLIENT" ]]; then
    LLPROMPT="$FG[232]$BG[053] %n.%m %{$reset_color%}"
else;
    LLPROMPT="$FG[255]$BG[053] %n %{$reset_color%}"
fi
PS1="${LLPROMPT}$BG[053]$FG[200] %~ %{$reset_color%} "


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

[[ -a ~/.zshrc.after ]] && source ~/.zshrc.after

[[ -a ~/.zshrc_alias ]] && source ~/.zshrc_alias
