from user_modules.functions import reset_django_project

aliases['setxkbmap'] = 'setxkbmap -option caps:escape'
aliases['qa'] = 'i3-msg exit'
aliases['scan'] = 'scangearmp2'
aliases['andri'] = 'neomutt -F ~/.mutt/mail1'
aliases['accounts'] = 'neomutt -F ~/.mutt/mail2'
aliases['djsearch'] = 'grep -r $VIRTUAL_ENV/lib/python*/site-packages/django/ -T -e'
aliases['vim'] = 'nvim'
