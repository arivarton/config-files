from prompt_toolkit.keys import Keys


@events.on_ptk_create
def custom_keybindings(bindings, **kw):
    handler = bindings.registry.add_binding

    @handler(Keys.ControlW)
    def run_ls(event):
        ls --group-directories-first
        event.cli.renderer.erase()
