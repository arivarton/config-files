from os.path import expanduser

$PATH.extend([expanduser('~/bin'), expanduser('~/.fzf/bin'),
              expanduser('~/.gem/ruby/2.5.0/bin/')])

$CASE_SENSITIVE_COMPLETIONS = 0

# LOCALE
$LANG = 'is_IS.UTF-8'

# Environment variables
$XONSH_COLOR_STYLE = 'monokai'
$XONSH_TRACEBACK_LOGFILE = expanduser('~/.logs/xonsh_traceback.log')
$VI_MODE = True
$TERM = 'rxvt-unicode-256color'
$COLORTERM = 'rxvt-unicode-256color'
$VISUAL = 'vim'
$EDITOR = 'vim'
$CDPATH = '$HOME/Development/python/:$HOME/Development/www/Django'
$LS_COLORS='rs=0:di=01;36:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:'
$FUZZY_PATH_COMPLETIONS = True
$SUBSEQUENCE_PATH_COMPLETION = False
$HISTCONTROL = 'ignoreerr, ignoredups'
$BROWSER = 'qutebrowser'

# Stamp env variables
$STAMP_MINIMUM_HOURS = 1
$STAMP_ORG_NR = 919009551
$STAMP_NAME = 'arivarton - Andri Vidar Tryggvason'
$STAMP_ADDRESS = 'Klepp stasjon'
$STAMP_ZIP_CODE = '4353'
$STAMP_PHONE = '+354 833 9005'
$STAMP_MAIL = 'faktura@arivarton.com'
$STAMP_ACCOUNT_NUMBER = '9685 27 95848'
$STAMP_WAGE_PER_HOUR = 350

# PROMPT
if 'DISPLAY' in ${...}:
  from user_modules.prompt import X_PROMPT, X_PROMPT_RIGHT
  $PROMPT = X_PROMPT
  $RIGHT_PROMPT = X_PROMPT_RIGHT
else:
  from user_modules.prompt import TTY_PROMPT
  $PROMPT = TTY_PROMPT
