import os
from user_modules.colors import *
from xonsh.prompt.env import env_name


def import_color(color_name):
    with open(os.path.expanduser('~/.Xresources'), 'r') as xresources:
        query = xresources.read()
    for color in query.replace('\t', ' ').split('\n'):
      if color.startswith('#define %s' % color_name):
          return color.split(' ')[2]
base_color = import_color('base_color')
highlight_color = import_color('highlight_color')
highlight_contrast = import_color('highlight_contrast')

X_PROMPT = '{BACKGROUND_%s}{%s} {user}@{hostname} {BACKGROUND_%s}{%s} {BLACK}{short_cwd} {BACKGROUND_%s}{%s}{NO_COLOR} ' % (base_color,highlight_contrast,highlight_color,base_color,base00,highlight_color)
X_PROMPT_RIGHT = '{BOLD_%s}{env_name: {}}{NO_COLOR}{branch_color}{curr_branch: {}}' % (base0A)

TTY_PROMPT = '{BACKGROUND_WHITE}{BLACK} {user}@{hostname}{env_name: in {}} {NO_COLOR}{BOLD_WHITE} {cwd}{branch_color}{curr_branch: {}} {prompt_end}{NO_COLOR} > '
