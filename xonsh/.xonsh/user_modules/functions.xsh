def reset_django_project():
    find_output = $(find . -path '*/migrations/*' -not -name '__init__.py' -not -path '*/.venv*' -or -iname 'db.sqlite3')
    print(find_output)
    yes = ['Y','y','']
    no = ['N','n']
    while True:
        catched_input = input('Delete these files? [Y/n]: ')
        if catched_input in yes:
            print('Deleting files.')
            delete_output = $(find . -path '*/migrations/*' -not -name '__init__.py' -not -path '*/.venv*' -delete -or -iname 'db.sqlite3' -delete)
            print(delete_output)
            break
        elif catched_input in no:
            print('Will not delete')
            break
        else:
            pass # Run again
#  read confirmation
#
#  case $confirmation in
#  y|Y|'')
#    echo 'Deleted.'
#    ;;
#  *)
#    echo 'Canceled.'
#    ;;
#  esac
